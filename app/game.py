
class TennisGame(object):

    def __init__(self):
        self.sets_counter = 1
        self.player1 = ""
        self.player2 = ""
        self.winner = ""
        self.player1_serve = False
        self.player2_serve = False
        self.advantage = 0
        self.deuce = False
        self.game_set = {'p1_state': 0,
                         'p2_state': 0}
        self.score_dict = {0: 0, 1: 15, 2: 30, 3: 40}
        self.game_score = {'p1': {1: 0, 2: 0, 3: 0}, 'p2': {1: 0, 2: 0, 3: 0}}

    def add_players(self, player1, player2):
        '''Method to add player details '''
        self.player1 = player1
        self.player2 = player2

    def player_serve(self, player):
        '''Update the status of the player who is
            currently serving the ball.'''
        if not isinstance(player, int):
            raise ValueError("Only integer is supported")
        if player not in (1, 2):
            raise ValueError("Input restricted to 1 and 2 only")
        if player == 1:
            self.player1_serve = True
            self.player2_serve = False
        else:
            self.player1_serve = False
            self.player2_serve = True

    def winner_of_next_point(self, player):
        '''increment the respective player's score and update the game_set dict

            Args:
                player: int, 1 for player1
                             2 for player2
        '''
        if not isinstance(player, int):
            raise ValueError("Only integer is supported")
        if player not in (1, 2):
            raise ValueError("Input restricted to 1 and 2 only")
        if player == 1:
            if not self.deuce:
                self.game_set['p1_state'] += 1
        else:
            if not self.deuce:
                self.game_set['p2_state'] += 1
        if self.game_set['p1_state'] == 3 and self.game_set['p2_state'] == 3 and not self.deuce:
            self.deuce = True
            print "DEUCE"
        return self.game_decision(player)

    def set_reset(self):
        self.player1_serve = False
        self.player2_serve = False
        self.advantage = 0
        self.deuce = False
        self.game_set = {'p1_state': 0,
                         'p2_state': 0}
        self.sets_counter += 1

    def display_score(self):
        print '\n'
        print "Current Score"
        print "-------------"
        print "Set".ljust(30), '1\t2\t3'
        print '-' * 50
        print '{} '.format(self.player1).ljust(30), '\t'.join([`i` for i in self.game_score['p1'].values()])
        print '{} '.format(self.player2).ljust(30), '\t'.join([`i` for i in self.game_score['p2'].values()])
        print '\n'

    def game_decision(self, player):
        game_over_flag = False
        if not self.deuce:
            if self.game_set['p1_state'] > 3 or self.game_set['p2_state'] > 3:
                self.game_score["p{}".format(player)][self.sets_counter] = 1
                game_over_flag = True
            else:
                print "Game Score: {}-{}".format(self.score_dict[self.game_set['p1_state']], self.score_dict[self.game_set['p2_state']])
        elif self.advantage == player:
            self.game_score["p{}".format(player)][self.sets_counter] = 1
            game_over_flag = True
        else:
            self.advantage = player
            print "Advantage for {}".format(eval('self.player' + str(player)))
        if game_over_flag:
            print "Set Over"
            self.display_score()
            self.set_reset()
            stats = self.check_match_stats()
            if stats:
                return True
        return False

    def check_match_stats(self):
        # stop the match and decide the winner
        if self.sets_counter == 4:
            p1 = sum(self.game_score['p1'].values())
            p2 = sum(self.game_score['p2'].values())
            if p1 > p2:
                self.winner = self.player1
            else:
                self.winner = self.player2
            print "Match Over. {} wins".format(self.winner)
            return True
        else:
            return False