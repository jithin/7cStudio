from game import TennisGame

if __name__ == "__main__":
    game_over = False
    p1 = raw_input('Enter the name of player 1:\t')
    p2 = raw_input('Enter the name of player 2:\t')
    game = TennisGame()
    game.add_players(p1, p2)
    serve = input(
        'Who starts the first serve? (1 for {}, 2 for {}):\t'.format(p1, p2))
    game.player_serve(serve)
    while not game_over:
        winner = input(
            'Winner of next point (1 for {}, 2 for {}):\t'.format(p1, p2))
        game_over = game.winner_of_next_point(winner)
