# INSTALL DEPENDENCIES

```sudo pip install requirements.txt```


## Run the Unit Tests
------------------------------------------------
Execute the following command from 7cStudio Directory

```nosetests tests/ --with-coverage```


## Run the program

```
cd app

python lets_play.py
```