import unittest

from app.game import TennisGame


class TestGame(unittest.TestCase):

    def setUp(self):
        print "...................called........."
        self.game = TennisGame()
        self.score_dict = {0: 0, 1: 15, 2: 30, 3: 40}

    def set_players(self):
        '''Populate the player details and initial serving details'''
        p1 = "Sam"
        p2 = "Joe"
        self.game.add_players(p1, p2)
        self.game.player_serve(1)

    def create_deuce_condition(self):
        '''Creae a Deuce condition for further testing'''
        self.set_players()
        self.assertEqual(self.score_dict[self.game.game_set['p1_state']], 0)
        self.game.winner_of_next_point(1)
        self.assertEqual(self.score_dict[self.game.game_set['p1_state']], 15)
        self.game.winner_of_next_point(1)
        self.assertEqual(self.score_dict[self.game.game_set['p1_state']], 30)
        self.game.winner_of_next_point(1)
        self.assertEqual(self.score_dict[self.game.game_set['p1_state']], 40)
        self.assertEqual(self.score_dict[self.game.game_set['p2_state']], 0)
        self.game.winner_of_next_point(2)
        self.assertEqual(self.score_dict[self.game.game_set['p2_state']], 15)
        self.game.winner_of_next_point(2)
        self.assertEqual(self.score_dict[self.game.game_set['p2_state']], 30)
        self.game.winner_of_next_point(2)
        self.assertEqual(self.score_dict[self.game.game_set['p2_state']], 40)

    def one_of_the_player_wins_match(self, player):
        key = 'p{}_state'.format(player)
        self.set_players()
        self.assertEqual(self.score_dict[self.game.game_set[key]], 0)
        self.game.winner_of_next_point(player)
        self.assertEqual(self.score_dict[self.game.game_set[key]], 15)
        self.game.winner_of_next_point(player)
        self.assertEqual(self.score_dict[self.game.game_set[key]], 30)
        self.game.winner_of_next_point(player)
        self.assertEqual(self.score_dict[self.game.game_set[key]], 40)
        self.game.winner_of_next_point(player)

    def test_adding_players(self):
        '''Test for setting players to our game'''
        p1 = "Sam"
        p2 = "Joe"
        self.game.add_players(p1, p2)
        self.assertEqual(p1, self.game.player1)
        self.assertEqual(p2, self.game.player2)

    def test_first_serve(self):
        '''Test for setting first serve details'''
        self.game.player_serve(1)
        self.assertEqual(self.game.player1_serve, True)
        self.assertEqual(self.game.player2_serve, False)
        self.game.player_serve(2)
        self.assertEqual(self.game.player1_serve, False)
        self.assertEqual(self.game.player2_serve, True)

    def test_serve_input_type_validation(self):
        '''Validate the input type of serving details'''
        self.assertRaises(ValueError, self.game.player_serve, 'player1')

    def test_serve_input_value_validation(self):
        '''Validate to accept only 1 and 2 as input for serving function '''
        self.assertRaises(ValueError, self.game.player_serve, 3)

    def test_winner_of_next_point(self):
        '''Winner of next point'''
        self.set_players()
        self.assertEqual(self.score_dict[self.game.game_set['p1_state']], 0)
        self.game.winner_of_next_point(1)
        self.assertEqual(self.score_dict[self.game.game_set['p1_state']], 15)
        self.assertEqual(self.score_dict[self.game.game_set['p2_state']], 0)
        self.game.winner_of_next_point(2)
        self.assertEqual(self.score_dict[self.game.game_set['p2_state']], 15)
        self.game.winner_of_next_point(2)
        self.assertEqual(self.score_dict[self.game.game_set['p2_state']], 30)
        self.game.winner_of_next_point(2)
        self.assertEqual(self.score_dict[self.game.game_set['p2_state']], 40)

    def test_winner_of_next_point_input_type_validation(self):
        self.assertRaises(
            ValueError, self.game.winner_of_next_point, 'player1')

    def test_winner_of_next_point_input_value_validation(self):
        self.assertRaises(ValueError, self.game.winner_of_next_point, 3)

    def test_for_deuce_condition(self):
        '''Create a Deuce condition: both player will be having 40 points each in a set '''
        self.create_deuce_condition()
        self.assertEqual(self.game.deuce, True)

    def test_for_deuce_advantage_condition(self):
        '''Create a Deuce condition: both player will be having 40 points each in a set'''
        # self.game.set_reset()
        self.set_players()
        self.create_deuce_condition()
        self.assertEqual(self.game.deuce, True)
        self.game.winner_of_next_point(1)
        self.assertEqual(self.game.advantage, 1)
        self.game.winner_of_next_point(2)
        self.assertEqual(self.game.advantage, 2)
        self.game.winner_of_next_point(1)
        self.assertEqual(self.game.advantage, 1)
        self.game.winner_of_next_point(1)
        self.assertEqual(self.game.game_score['p1'][1], 1)

    def test_set_over_condition(self):
        '''If set got over update the main score list and reset the flags for 
           duece, advantage, game_set and increment the set_counter by one
        '''
        # self.game.set_reset()
        self.one_of_the_player_wins_match(player=1)
        self.assertEqual(self.game.game_score['p1'][1], 1)
        self.one_of_the_player_wins_match(player=2)
        self.assertEqual(self.game.game_score['p2'][2], 1)
        self.one_of_the_player_wins_match(player=2)
        self.assertEqual(self.game.game_score['p2'][3], 1)
        self.assertEqual(self.game.deuce, False)
        self.assertEqual(self.game.advantage, 0)
        self.assertEqual(self.game.game_set, {'p1_state': 0,
                                              'p2_state': 0})

    def test_match_winner_player1(self):
        '''Perform 3 sets and identify the winner of the match '''
        self.one_of_the_player_wins_match(player=1)
        self.one_of_the_player_wins_match(player=2)
        self.one_of_the_player_wins_match(player=1)
        # here player1 wins the match
        self.assertEqual(self.game.winner, self.game.player1)

    def test_match_winner_player2(self):
        # resetting game_score and counter
        self.game = TennisGame()
        self.one_of_the_player_wins_match(player=1)
        self.one_of_the_player_wins_match(player=2)
        self.one_of_the_player_wins_match(player=2)
        # here player2 wins the match
        self.assertEqual(self.game.winner, self.game.player2)


if __name__ == "__main__":
    unittest.main()
